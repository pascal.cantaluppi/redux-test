// Counter.js
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { decrement, increment } from './counterSlice';

function Counter() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  return (
    <div>
      <div>
        <span style={{ fontSize: '200px' }}>{count}</span>
      </div>
      <div>
        <button onClick={() => dispatch(increment())}>increment</button>
        &nbsp;
        <button onClick={() => dispatch(decrement())}>decrement</button>
      </div>
    </div>
  );
}

export default Counter;
